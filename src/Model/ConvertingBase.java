package Model;

/**
 *
 * @author anhnb
 */
public class ConvertingBase {

    private int baseIn;
    private int baseOut;

    public ConvertingBase(int baseIn, int baseOut) {
        this.baseIn = baseIn;
        this.baseOut = baseOut;
    }

    private String decToBase(int n, int base) {
            if (n < 0 || base < 2 || base > 16) {
            return "";
        }

        
        StringBuilder sb = new StringBuilder();
        int remainder = n;
        int m;
        if (n == 0) {
            return "0";
        }

        while (remainder > 0) {
            m = remainder % base;
            remainder = remainder / base;
            if (m > 9) {
                sb.append((char) (55 + m));
            } else {
                sb.append(m);

            }
        }

        return sb.reverse().toString();
    }

    private int baseToDec(String n, int base) {
        n = new StringBuilder(n).reverse().toString();
        int num = 0;
        char c;
        int hashCode;
        //0101
        for (int i = 0; i < n.length(); i++) {
            c = n.charAt(i);
            if (Character.isDigit(c)) {
                num += Integer.parseInt(c + "") * Math.pow(base, i);
            } else {
                hashCode = Character.hashCode(c) - 55;
                num += hashCode * Math.pow(base, i);
            }

        }
        return num;
    }
    public String inToOut(String n){
        int dec = baseToDec(n, baseIn);
        return decToBase(dec, baseOut);
    }
    
}
