/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author anhnb
 */
public class Management {

    public Management() {
    }

    public String decToBase(int n, int base) {
        StringBuilder sb = new StringBuilder();
        int remainder = n;
        int m;
        if (n == 0) {
            return "0";
        }

        while (remainder > 0) {
            m = remainder % base;
            remainder = remainder / base;
            if (m > 9) {
                sb.append((char) (55 + m));
            } else {
                sb.append(m);

            }
        }

        return sb.reverse().toString();
    }

    public int baseToDec(String n, int base) {
        n = new StringBuilder(n).reverse().toString();
        int num = 0;
        char c;
        int hashCode;
        //0101
        for (int i = 0; i < n.length(); i++) {
            c = n.charAt(i);
            if (Character.isDigit(c)) {
                num += Integer.parseInt(c + "") * Math.pow(base, i);
            } else {
                hashCode = Character.hashCode(c) - 55;
                num += hashCode * Math.pow(base, i);
            }

        }
        return num;

    }
}
